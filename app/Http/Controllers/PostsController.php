<?php

namespace App\Http\Controllers;

use App\Posts;
use Illuminate\Http\Request;
use App\User;
use App\Http\Controllers\FacebookAuthController;
use Laravel\Socialite\Facades\Socialite;
use League\Flysystem\Exception;


class PostsController extends Controller
{

    public function index()
    {

        $user = $this->FacebookUser()->get('name');

        if ($this->checkNewPost()) {
            $postData = $this->getPostData($this->PostsId());
            $this->savePostData($postData);
        } else {
            $postData = Posts::where('user_id', '=', auth()->user()->id)->get();
        }


        return view('posts', compact('user', 'postData'));
    }

    public function getUser()
    {
        return User::find(['id' => auth()->user()->id]);
    }

    public function getUserToken($user)
    {
        return $accessToken = $user['0']['token'];
    }

    public function PostsId()
    {
        $node = '/' . $this->FacebookUser()->get('id') . '/posts?limit=5';

        $response = FacebookAuthController::FB()->get($node, $this->getUserToken($this->getUser()));

        $rawArray = $response->getDecodedBody();

        if (!isset($rawArray['data'])){throw new Exception('FaceBook не вернул данных');}

        for ($i = 0; $i <= 4; $i++) {
            $postId[] = $rawArray['data'][$i]['id'];
        }

        return $postId;
    }

    public function getPostData(array $postId)
    {
        for ($i = 0; $i <= 4; $i++) {
            $node = '/' . $postId[$i] . '/?fields=picture,likes,permalink_url,shares,message,created_time';

            $response = FacebookAuthController::FB()->get($node, $this->getUserToken($this->getUser()));
            $rawArray = $response->getDecodedBody();

            $postData[$i] = [
                'likeCount' => isset($rawArray['likes']) ? count($rawArray['likes']) : 0,
                'shares' => isset($rawArray['shares']) ? $rawArray['shares']['count'] : 0,
                'message' => isset($rawArray['message']) ? strlen($rawArray['message']) > 200 ? substr($rawArray['message'], 0, 200) . '...' : $rawArray['message'] : 'No Message',
                'picture' => isset($rawArray['picture']) ? $rawArray['picture'] : '/noimage.png',
                'link' => $rawArray['permalink_url'],
                'created_time' => date('d F Y', strtotime($rawArray['created_time'])),
                'post_id' => $postId[$i],
                'user_id' => auth()->user()->id,
            ];
        }
        return $postData;
    }

    public function savePostData(array $postData)
    {
        Posts::where('user_id', '=', auth()->user()->id)->delete();

        for ($i = 0; $i <= 4; $i++) {
            $post = new Posts;
            $post->fill([
                'user_id' => $postData[$i]['user_id'],
                'post_id' => $postData[$i]['post_id'],
                'link' => $postData[$i]['link'],
                'likes' => $postData[$i]['likeCount'],
                'shares' => $postData[$i]['shares'],
                'picture' => $postData[$i]['picture'],
                'message' => $postData[$i]['message'],
                'created_time' => $postData[$i]['created_time']
            ])->save();

        }

    }

    public function checkNewPost()
    {

        $localPost = Posts::where('user_id', auth()->user()->id)->first();

        $node = '/' . $this->FacebookUser()->get('id') . '/posts?limit=1';

        $response = FacebookAuthController::FB()->get($node, $this->getUserToken($this->getUser()));
        $rawArray = $response->getDecodedBody();

        $localPost = isset($localPost->post_id) ? $localPost->post_id : 0;

        if ($localPost == $rawArray['data']['0']['id']) {
            return false;
        }
        return true;
    }

    public function FacebookUser()
    {
        $response = FacebookAuthController::FB()->get('/me', $this->getUserToken($this->getUser()));
        return $user = collect($response->getDecodedBody());
    }

}
