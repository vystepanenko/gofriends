<?php

namespace App\Http\Controllers;


use Laravel\Socialite\Facades\Socialite;
use App\User;


class FacebookAuthController extends Controller
{
    public function redirect()
    {
        return Socialite::driver('facebook')->redirect();
    }

    public function callback()
    {
        $user = $this->findOrCreateFacebookUser(
            Socialite::driver('facebook')->user()
        );

        auth()->login($user);

        return redirect('/posts');


    }

    public function findOrCreateFacebookUser($FacebookUser)
    {
        $user = User::firstOrNew(['facebook_id' => $FacebookUser->id]);

        if ($user->exists & $FacebookUser->token == $user->token) return $user;

        $user->fill([
            'name' => bcrypt($FacebookUser->name),
            'email' => $FacebookUser->email,
            'token' => $FacebookUser->token,
        ])->save();

        return $user;
    }

    static public function FB()
    {
        return new \Facebook\Facebook([
            'app_id' => env('FACEBOOK_CLIENT_ID'),
            'app_secret' => env('FACEBOOK_CLIENT_SECRET'),
            'default_graph_version' => 'v2.10',
        ]);
    }
}

