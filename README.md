# README #

Steps are necessary to get your application up and running.

### What is this repository for? ###

Test task for GoFriends

### How do I get set up? ###

1. Git
2. PHP
3. Vagrant
4. VirtualBox
5. Composer
6. git clone git@bitbucket.org:vystepanenko/gofriends.git
7. composer install
8. Mac / Linux:
	php vendor/bin/homestead make
   Windows:
	vendor\\bin\\homestead make
9. Edit C:\Windows\System32\drivers\etc\hosts.
		192.168.10.10  homestead.app
10. vagrent up


### Test user ###
user: bob_hsfegex_wisemansky@tfbnw.net
Password: GoFriends