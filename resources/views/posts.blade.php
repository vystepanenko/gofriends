<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>GoFriends Test Task</title>

    <link href="https://v4-alpha.getbootstrap.com/dist/css/bootstrap.min.css" rel="stylesheet">
    <style>
        table {
            border: 0px solid grey;
            border-collapse: collapse;
            width: 100%;
            height: 300px;
        }

        th, td {
            border: 1px solid black;
            padding: 5px;
            text-align: center;
        }

        .jumbotron {
            padding-top: 10px;
            padding-bottom: 10px;
            background-size: cover;
            height: 200px;
            text-align: center;
        }
    </style>
</head>

<body>

<div class="jumbotron">
    <div class="container">
        <p>
        <div class="content">
            <div class="title m-b-md">
                <form action="/logout">
                    <input type="submit"
                           value="DisConnect from Facebook"
                           class="btn btn-primary btn-lg"
                    >
                </form>
            </div>
        </div>
        </p>
        <h1 class="display-3">{{$user}}!</h1>
    </div>
</div>

<div class="container">
    <!-- Example row of columns -->
    <div class="row">
        <div class="col-md-4">
            <table>
                <tr>
                    <th colspan="5">{{ $postData['0']['created_time'] }}</th>
                </tr>
                <tr>
                    <td colspan="5">
                        <img src="{{$postData['0']['picture'] }}" alt=""></td>
                </tr>
                <tr>
                    <td colspan="5">{{$postData['0']['message']}}</td>
                </tr>
                <tr>
                    <td>Likes: {{$postData['0']['likeCount']}}</td>
                    <td></td>
                    <td>
                        Shares: {{ $postData['0']['shares'] }}
                    </td>
                    <td></td>
                    <td>
                        <a href="{{$postData['0']['link']}}">Link to Facebook</a></td>
                </tr>
            </table>
        </div>
        <div class="col-md-4">
            <table>
                <tr>
                    <th colspan="5">{{ $postData['1']['created_time'] }}</th>
                </tr>
                <tr>
                    <td colspan="5">
                        <img src="{{$postData['1']['picture'] }}" alt=""></td>
                </tr>
                <tr>
                    <td colspan="5">{{$postData['1']['message']}}</td>
                </tr>
                <tr>
                    <td>Likes: {{$postData['1']['likeCount']}}</td>
                    <td></td>
                    <td>
                        Shares: {{ $postData['1']['shares'] }}
                    </td>
                    <td></td>
                    <td>
                        <a href="{{$postData['1']['link']}}">Link to Facebook</a></td>
                </tr>
            </table>
        </div>
        <div class="col-md-4">
            <table>
                <tr>
                    <th colspan="5">{{ $postData['2']['created_time'] }}</th>
                </tr>
                <tr>
                    <td colspan="5">
                        <img src="{{$postData['2']['picture'] }}" alt=""></td>
                </tr>
                <tr>
                    <td colspan="5">{{$postData['2']['message']}}</td>
                </tr>
                <tr>
                    <td>Likes: {{$postData['2']['likeCount']}}</td>
                    <td></td>
                    <td>
                        Shares: {{ $postData['2']['shares'] }}
                    </td>
                    <td></td>
                    <td>
                        <a href="{{$postData['2']['link']}}">Link to Facebook</a></td>
                </tr>
            </table>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-md-6">
            <table>
                <tr>
                    <th colspan="5">{{ $postData['3']['created_time'] }}</th>
                </tr>
                <tr>
                    <td colspan="5"><img src="{{$postData['3']['picture'] }}" alt=""></td>
                </tr>
                <tr>
                    <td colspan="5">{{$postData['3']['message']}}</td>
                </tr>
                <tr>
                    <td>Likes: {{$postData['2']['likeCount']}}</td>
                    <td></td>
                    <td>
                        Shares: {{ $postData['3']['shares'] }}
                    </td>
                    <td></td>
                    <td>
                        <a href="{{$postData['3']['link']}}">Link to Facebook</a></td>
                </tr>
            </table>
        </div>
        <div class="col-md-6">
            <table>
                <tr>
                    <th colspan="5">{{ $postData['4']['created_time'] }}</th>
                </tr>
                <tr>
                    <td colspan="5">
                        <img src="{{$postData['4']['picture'] }}" alt=""></td>
                </tr>
                <tr>
                    <td colspan="5">{{$postData['4']['message']}}</td>
                </tr>
                <tr>
                    <td>Likes: {{$postData['2']['likeCount']}}</td>
                    <td></td>
                    <td>
                        Shares: {{ $postData['4']['shares'] }}
                    </td>
                    <td></td>
                    <td>
                        <a href="{{$postData['4']['link']}}">Link to Facebook</a></td>
                </tr>
            </table>
        </div>
    </div>

</div> <!-- /container -->


<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://code.jquery.com/jquery-3.1.1.slim.min.js"
        integrity="sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n"
        crossorigin="anonymous"></script>
<script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"
        integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb"
        crossorigin="anonymous"></script>
</body>
</html>
