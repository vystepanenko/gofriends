<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>GoFriends Test Task</title>

    <link href="https://v4-alpha.getbootstrap.com/dist/css/bootstrap.min.css" rel="stylesheet">

    <style>

        .jumbotron {
            padding-top: 5px;
            padding-bottom: 10px;
            background-size: cover;
            height: 100vh;
            width: 100%;
            text-align: center;
        }
    </style>
</head>

<body>
<div class="jumbotron vertical-center">
    <div class="content">
        <div class="title m-b-md">
            <form action="/redirect">
                <input type="submit"
                       value="Connect to Facebook"
                       class="btn btn-primary btn-lg"
                >
            </form>
        </div>
        <h1 class="display-3">Welcome to GoFriends test task :)</h1>
    </div>
</div>
</body>
</html>
